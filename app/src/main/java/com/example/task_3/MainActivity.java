package com.example.task_3;

/*

1. Создать приложение с одним экраном. Добавить на экран поле для ввода текста (EditText) и две кнопки.
По нажатию на первую кнопку с текстового поля весь введенный текст исчезает.
По нажатию на вторую кнопку поле ввода восстанавливает текст, удаленный первой кнопкой.

1.1* По повторному нажатию на вторую кнопку, текстовое поле восстанавливает текст, введенный ранее.
(Например: введено слово "мышь" -> поле очищено -> введено слово "хомяк" -> поле очищено -> нажата восстанавливающая кнопка, появился текст "хомяк" -> нажата
восстанавливающая кнопка еще раз -> появился текст "мышь").
Реализовать сохранение/восстановление пяти состояний.

*/

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    private static final String KEY_INPUT_TEXT = "inputText";

    EditText txtInputText;
    Button btnClearText;
    Button btnRestoreText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtInputText = findViewById(R.id.txtInputText);
        btnClearText = findViewById(R.id.btnClearText);
        btnRestoreText = findViewById(R.id.btnRestoreText);

        final Stack<String> stack = new Stack<>();

        btnRestoreText.setEnabled(false);

/*
        if (savedInstanceState != null)
            txtInputText.setText(savedInstanceState.getString(KEY_INPUT_TEXT));
*/

        txtInputText.requestFocus();

/*
        txtInputText.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event){


                return true;
            }
        });
*/


        btnClearText.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                if (txtInputText.getText().toString().trim().isEmpty()) {
                                                    Toast.makeText(getApplicationContext(), "Input Text", Toast.LENGTH_LONG).show();
                                                    txtInputText.requestFocus();
                                                    return;
                                                }

                                                // Save input text
//              txtRestoreText = txtInputText.getText().toString().trim();

                                                stack.push(txtInputText.getText().toString().trim());

                                                txtInputText.setText("");
                                                btnRestoreText.setEnabled(true);

                                            }
                                        }
        );

        btnRestoreText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!stack.empty())
                    txtInputText.setText(stack.pop());
            }
        });
    }

/*
    @Override
    public void onSaveInstanceState(Bundle saveInstanceState) {
        super.onSaveInstanceState(saveInstanceState);

        saveInstanceState.putString(KEY_INPUT_TEXT, txtInputText.getText().toString().trim());
    }
*/

}
